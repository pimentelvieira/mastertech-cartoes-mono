package com.mastertechcartoesmono.controllers;

import com.mastertechcartoesmono.models.Cartao;
import com.mastertechcartoesmono.models.Cliente;
import com.mastertechcartoesmono.models.Pagamento;
import com.mastertechcartoesmono.services.CartaoService;
import com.mastertechcartoesmono.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<Pagamento> salvarPagamento(@RequestBody Pagamento pagamento) {
        Optional<Cartao> cartao = this.cartaoService.getCartaoById(pagamento.getCartaoId());

        if (cartao.isPresent()) {
            Pagamento pagamentoSalvo = this.pagamentoService.salvarPagamento(pagamento);
            return ResponseEntity.status(HttpStatus.CREATED).body(pagamento);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão não encontrado");
        }
    }

    @GetMapping
    public List<Pagamento> findAll() {
        return this.pagamentoService.findAll();
    }

    @GetMapping("/{id}")
    public List<Pagamento> findAllByCartaoId(@PathVariable Integer id) {
        return this.pagamentoService.findAllByCartaoId(id);
    }
}
