package com.mastertechcartoesmono.controllers;

import com.mastertechcartoesmono.models.Cartao;
import com.mastertechcartoesmono.models.Cliente;
import com.mastertechcartoesmono.services.CartaoService;
import com.mastertechcartoesmono.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cartao> salvarCartao(@RequestBody Cartao cartao) {
        Optional<Cliente> cliente = this.clienteService.getClienteById(cartao.getClienteId());
        if (cliente.isPresent()) {
            cartao.setAtivo(Boolean.FALSE);
            Cartao cartaoSalvo = this.cartaoService.salvarCartao(cartao);
            return ResponseEntity.status(HttpStatus.CREATED).body(cartao);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
        }
    }

    @GetMapping("/{numero}")
    public ResponseEntity<Cartao> getCartaoById(@PathVariable String numero) {
        Optional<Cartao> result = this.cartaoService.getCartaoByNumero(numero);

        if (result.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(result.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão não encontrado");
        }
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<Cartao> ativarDesativarCartao(@RequestBody Cartao cartao, @PathVariable String numero) {
        Optional<Cartao> result = this.cartaoService.getCartaoByNumero(numero);

        if (result.isPresent()) {
            Cartao cartaoAtualizado = result.get();
            cartaoAtualizado.setAtivo(cartao.isAtivo());
            cartaoAtualizado = this.cartaoService.salvarCartao(cartaoAtualizado);
            return ResponseEntity.status(HttpStatus.OK).body(cartaoAtualizado);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão não encontrado");
        }
    }
}
