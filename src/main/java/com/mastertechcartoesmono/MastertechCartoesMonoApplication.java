package com.mastertechcartoesmono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechCartoesMonoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechCartoesMonoApplication.class, args);
	}

}
