package com.mastertechcartoesmono.repositories;

import com.mastertechcartoesmono.models.Cartao;
import com.mastertechcartoesmono.models.Pagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    public Iterable<Pagamento> findAllByCartaoId(Integer id);
}
