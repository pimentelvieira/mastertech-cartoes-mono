package com.mastertechcartoesmono.repositories;

import com.mastertechcartoesmono.models.Cartao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    public Optional<Cartao> findByNumero(String numero);
}
