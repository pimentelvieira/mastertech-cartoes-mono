package com.mastertechcartoesmono.services;

import com.mastertechcartoesmono.models.Pagamento;
import com.mastertechcartoesmono.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Pagamento salvarPagamento(Pagamento pagamento) {
        return this.pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> findAll() {
        return (List<Pagamento>) this.pagamentoRepository.findAll();
    }

    public List<Pagamento> findAllByCartaoId(Integer id) {
        return (List<Pagamento>) this.pagamentoRepository.findAllByCartaoId(id);
    }
}
