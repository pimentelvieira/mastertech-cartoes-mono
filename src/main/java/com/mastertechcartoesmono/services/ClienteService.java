package com.mastertechcartoesmono.services;

import com.mastertechcartoesmono.models.Cliente;
import com.mastertechcartoesmono.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente) {
        return this.clienteRepository.save(cliente);
    }

    public Optional<Cliente> getClienteById(Integer id) {
        return this.clienteRepository.findById(id);
    }
}
