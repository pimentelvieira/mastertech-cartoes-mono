package com.mastertechcartoesmono.services;

import com.mastertechcartoesmono.models.Cartao;
import com.mastertechcartoesmono.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao salvarCartao(Cartao cartao) {
        return this.cartaoRepository.save(cartao);
    }

    public Optional<Cartao> getCartaoByNumero(String numero) {
        return this.cartaoRepository.findByNumero(numero);
    }

    public Optional<Cartao> getCartaoById(Integer id) {
        return this.cartaoRepository.findById(id);
    }
}
