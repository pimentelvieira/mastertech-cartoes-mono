# Read Me First
The following was discovered as part of building this project:

* The original package name 'com.mastertech-cartoes-mono' is invalid and this project uses 'com.mastertechcartoesmono' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.8.RELEASE/maven-plugin/)

